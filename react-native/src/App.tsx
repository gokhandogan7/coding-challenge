import { Provider } from "react-redux";
import { configureStore } from "./store";
import React from "react";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { RootNavigator } from "./navigation/RootNavigator";

const store = configureStore();

export default () => (
  <Provider store={store}>
    <SafeAreaProvider>
      <RootNavigator />
    </SafeAreaProvider>
  </Provider>
);
