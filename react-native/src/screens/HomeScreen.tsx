import { MaterialCommunityIcons } from "@expo/vector-icons";
import React, { useEffect } from "react";
import { StyleSheet, View, FlatList } from "react-native";
import { Appbar, FAB } from "react-native-paper";
import { useSelector, useDispatch } from "react-redux";
import { selectors, actions, Inventory } from "../store/inventory";
import { RootState } from "../store";
import { SafeAreaView } from "react-native-safe-area-context";
import { StackScreenProps } from "@react-navigation/stack";
import { ProductItem } from "../components/ProductItem";
import { useNavigation } from "@react-navigation/native";
import { MainRoutes } from "../navigation/config";
import { useFonts } from "expo-font";

export default (props: StackScreenProps<{}>) => {
  const [loaded] = useFonts({
    "Roboto-Medium": require("../assets/fonts/Roboto-Medium.ttf")
  });
  const navigation = useNavigation();
  const fetching = useSelector((state: RootState) => state.inventory.fetching);
  const inventory = useSelector(selectors.selectInventory);
  const dispatch = useDispatch();
  const uniqueInventory = Object.values(
    inventory.reduce((o, t) => ({ ...o, [t.fields["Product Name"]]: t }), {})
  ) as Inventory[];

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      dispatch(actions.fetchInventory());
    });
    return unsubscribe;
  }, [navigation]);

  const renderItem = ({ item }: { item: Inventory }) => {
    return <ProductItem {...item} />;
  };

  if (!loaded) {
    return null;
  }

  return (
    <View style={styles.appbarContainer}>
      <Appbar.Header style={styles.appbarHeader}>
        <Appbar.Content titleStyle={styles.appbarContent} title="Inventory" />
      </Appbar.Header>

      <FlatList
        data={uniqueInventory}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
        onRefresh={() => dispatch(actions.fetchInventory())}
        refreshing={fetching}
        onEndReached={() => dispatch(actions.fetchMoreInventory(100))}
        style={styles.flatlist}
      />

      <SafeAreaView style={styles.fab}>
        <FAB
          icon={() => (
            <MaterialCommunityIcons name="barcode" size={24} color="#0B5549" />
          )}
          label="Scan Product"
          onPress={() => navigation.navigate(MainRoutes.CAMERA)}
        />
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  fab: {
    position: "absolute",
    bottom: 16,
    width: "100%",
    flex: 1,
    alignItems: "center"
  },

  appbarContainer: {
    backgroundColor: "#e5e5e5",
    flex: 1
  },
  appbarHeader: {
    alignItems: "flex-end",
    justifyContent: "center",
    height: 100
  },
  appbarContent: {
    alignSelf: "center",
    paddingBottom: 20,
    fontWeight: "400"
  },
  flatlist: {
    paddingTop: 16
  }
});
