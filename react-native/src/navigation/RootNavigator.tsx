import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import { MainRoutes } from "./config";
import HomeScreen from "../screens/HomeScreen";
import CameraScreen from "../screens/CameraScreen";

export type MainParams = {
  [MainRoutes.HOME]: undefined;
  [MainRoutes.CAMERA]: undefined;
};

const Stack = createStackNavigator<MainParams>();
export const RootNavigator = () => (
  <NavigationContainer>
    <Stack.Navigator
      initialRouteName={MainRoutes.HOME}
      screenOptions={{
        headerShown: false
      }}
    >
      <Stack.Screen name={MainRoutes.HOME} component={HomeScreen} />
      <Stack.Screen name={MainRoutes.CAMERA} component={CameraScreen} />
    </Stack.Navigator>
  </NavigationContainer>
);
