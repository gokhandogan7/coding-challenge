import React from "react";
import { Inventory } from "../store/inventory";
import { Card, Title, Paragraph, Badge, Text } from "react-native-paper";
import { StyleSheet, View } from "react-native";
import { formatDate, checkStatus} from "../helpers";


export const ProductItem = ({ createdTime, fields }: Inventory) => {
  const isNew = checkStatus(fields)
  const isUrlAvailable = !!fields['Product Image'];
  const createdAt = formatDate(new Date(createdTime));
  const badges = fields["Product Categories"]?.split(",");

  const coverStyle = isUrlAvailable
    ? styles.cardCover
    : styles.cardCoverPlaceholder;
  const source = isUrlAvailable
    ? {uri: fields['Product Image']}
    : require('../img/Vector.png');

  if (!fields["Product Name"]) {
    return null;
  }
  return (
    <Card style={styles.cardContainer}>
      <Card.Content style={styles.cardContentContainer}>
      <Card.Cover source={source} style={coverStyle} />
        <Card.Content style={styles.titleContainer}>
          <View style={styles.titleContentWrapper}>
            <View style={styles.titleContent}>
              <Title style={styles.title}>{fields["Product Name"]}</Title>
              <Paragraph style={styles.paragraph}>{createdAt}</Paragraph>
            </View>
            {isNew && (
              <View style={styles.newLabel}>
                <Text style={styles.newText}>New</Text>
              </View>
            )}
          </View>

          <Card.Content style={styles.badgeWrapper}>
            {badges?.map((item, i) => (
              <Badge key={i.toString()} style={styles.badge}>
                {item}
              </Badge>
            ))}
          </Card.Content>
        </Card.Content>
      </Card.Content>
    </Card>
  );
};

const styles = StyleSheet.create({
  cardContainer: {
    flexDirection: "row",
    marginHorizontal: 16,
    marginVertical: 6,
    elevation: 3,
    shadowColor: "#1b2633",
    backgroundColor: "#f8f9fc",
    borderRadius: 4
  },
  cardContentContainer: {
    flexDirection: "row",
    margin: 0,
    paddingHorizontal: 8,
    paddingVertical: 8
  },

  cardCover: {
    width: 85,
    height: "100%",
    minHeight: 112
  },
  cardCoverPlaceholder: {
    width: 85,
    height: 85,
  },

  titleContainer: {
    flexDirection: "column",
    flex: 1,
    margin: 0,
    paddingRight: 0
  },
  titleContentWrapper: {
    flexDirection: "row"
  },
  titleContent: {
    flex: 1
  },
  title: {
    lineHeight: 22,
    fontSize: 20,
    marginBottom: 0,
    color: "#1B2633",
    fontFamily: "Roboto-Medium",
    fontWeight: "900"
  },
  paragraph: {
    fontWeight: "400",
    fontFamily: "Roboto-Medium",
    color: "#1B2633",
    marginTop: 0,
    fontSize: 12,
    lineHeight: 16
  },

  newLabel: {
    backgroundColor: "#333333",
    height: 34,
    width: 53,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 9,
    borderTopRightRadius: 0
  },
  newText: { color: "#fff" },

  badgeWrapper: {
    marginTop: 12,
    flexDirection: "row",
    marginLeft: 0,
    paddingLeft: 0,
    flexWrap: "wrap",
    width: "100%"
  },
  badge: {
    backgroundColor: '#d4e5ff',
    paddingHorizontal: 12,
    marginVertical: 4,
    marginRight: 3,
    fontSize: 12,
    fontFamily: 'Roboto',
  },
});
