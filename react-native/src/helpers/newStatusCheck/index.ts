interface IFields {
    Posted: string;
    "Product Code": string;
    "Product Name": string;
    "Product Image": string;
    "Product Categories": string;
  }
  
  
  export const checkStatus = (fields: IFields ) => {
    const date1 = new Date();
    const date2 = new Date(fields.Posted);
    const diffTime = Math.abs(date2.getTime() - date1.getTime());
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    const isNew = diffDays < 7;
    
    return isNew
  }