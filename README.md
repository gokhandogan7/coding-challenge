# Coding Challenge

<p>
  <img src="https://d2zr9w65gdacs9.cloudfront.net/58590/thumbnail1616164169logo.png" height="100" />
</p>

## About App
Inventory is a small utility app that could be used in a logistics company. It has two use cases:

1. Adding products to the inventory using a bar code scanner 
2. Viewing the inventory of products  

Screenshots: 

<img src="./HomeScreen.jpeg" height=480>
<img src="./CameraScreen.jpg" height=480>

This repository implemented with: 

* React Native: Expo, TypeScript, React Native Paper and Redux. 

The backend is a REST API provided by Airtable.


## Product Requirements

- [X] Show name (truncated to 1 line)
- [X] Show date
- [X] If date is within last 7 days, show a "New" icon 
- [X] Show image
- [X] If image is missing, show a placeholder
- [X] Show categories as individual "Tags"
- [X] Create a `fetchMoreInventory` thunk
  - [X] Enables pagination using the `offset` parameter

---
**Tipp:** 
The new icon will not appear on products older than 7 days. If you want to see how the New icon looks like, you can change the "isNew" value in src>helpers>newStatusCheck>index.ts.


## Context

The project is divided into 2 parts.

- ionic-angular 

- react-native 

## Instructions

- Clone this repo.
- Open the project folder using Visual Studio Code

### To Start "react-native"
                 
1. Open a new terminal
2. cd to folder "react-native"
3. Execute "npm install" to get the dependencies
4. Execute "npm run android" or "npm run ios" to start the project.

\*
-If you have a problem with expo packages,then execute npm update or you can follow the steps in the link below. (I got this error, expo emulator was not working older than sdk 43.0.0 version.)

**Link:** 
https://docs.npmjs.com/updating-packages-downloaded-from-the-registry
\*



### That's it!

Salute!

<img src="https://c.tenor.com/y2JXkY1pXkwAAAAC/cat-computer.gif" width="200" height="200" />
